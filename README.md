# Wii U Homebrew Launcher
Welcome to another repository by MCMiners9! This time, the repository is about homebrew for Wii U! [Click here](https://github.com/MCMiners9/homebrew_wiiu/releases) to download Homebrew (it comes with DDD, Loadiine GX2, ftpiiu, Meme Run game dump, Splatoon empty game dump folder, and more)

## How do I setup Homebrew for Wii U?

1. Download Homebrew from the link above
2. Extract the ZIP
3. Insert a SD card (recommended size: at least 16 GB)
4. Go to This PC or wherever you go to view all the drives inserted into your PC.
5. Right click the SD card and click format. NOTE: FORMATTING WILL DELETE ALL DATA ON THE SD CARD. PLEASE MAKE SURE YOU HAVE BACKUPS!
6. You need to format the drive in FAT32 format.
7. When the format is finished, copy the wiiu folder from the ZIP you extractde earlier into the SD card.
8. You have now setup the SD card for Homebrew.

## How do I use Homebrew on Wii U?

1. On your Wii U, go to the Internet Browser.
2. Type `loadiine.ovh` into the address bar.
3. From the dropdown, select Homebrew Launcher (not Illuminati Homebrew)
4. Let it load the Homebrew.
5. You are now in the Homebrew launcher.

### Major credits to the creator of the Wii U Homebrew Launcher, DDD, ftpiiu, Loadiine GX2, Geckiine.

## Report bugs to me by clicking [here](https://github.com/MCMiners9/homebrew_wiiu/issues)
