#This is how your SD card path should look if you made the folders in the correct order:

`D:/wiiu/games/Example Game [123456]`

and

`D:/wiiu/apps/Example_App`

## The apps folder should have the following inside: `homebrew_launcher, loadiine_gx2, ddd, ftpiiu.`

## Each app name folder has to have a `.ELF` file, icon file, and `meta.xml` file inside it. This is an exception for `loadiine_gx2` since loadiine_gx2 has more files.

## Do not make your own `code`, `content`, and `meta` folders for a game. Just make a folder in `wiiu/games` naming it after the game you want to dump, and also placing a id in [<--these things-->]. If you need a ID, google `"game name" "your country" ID.`

## Replace `D:/` with whatever letter your SD card uses.
